/// @description When hit
// You can write your code in this editor

// Remove from compass enemy array

if(bisDieTouch)
{
	with(compass_obj)
	{
		enemy_cnt -= 1;
		event_user(1);
	}

	audio_play_sound(snd_enemy_hit, 10, false);
	instance_destroy();
}