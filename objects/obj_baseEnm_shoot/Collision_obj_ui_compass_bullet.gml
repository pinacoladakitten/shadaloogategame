/// @description When hit
// You can write your code in this editor

// Damage parent if hit
if(parentChar != "")
{
	show_debug_message("Damaging: " + string(parentChar));
	parentChar.bisAttackSuccess = false;
}

// Remove from compass enemy array
with(compass_obj)
{
	enemy_cnt -= 1;
	event_user(1);
}

audio_play_sound(snd_enemy_hit, 10, false);
instance_destroy();