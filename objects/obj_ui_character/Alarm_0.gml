// Inherited
event_inherited();

// Reset the alarm to do this function again indefinitely
alarm_set(0, room_speed * 0.75);

// Set the direction for the bounce
portBounce *= -1;

// Debug shit...
//show_debug_message(room_speed);