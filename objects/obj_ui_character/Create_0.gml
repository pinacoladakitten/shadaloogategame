// Execute Inherited stuff
event_inherited()

// Set the portrait bounce height diff
portBounce = 2;

// Set an alarm to do the port bounce
alarm_set(0, room_speed * 0.75);

// Init General Character Stuff
name = "Yourself";
hp = 20;
mp = 10;

// Weapons
mainHand = instance_create_depth(0, 0, 0, obj_equip_fists);
offHand = instance_create_depth(0, 0, 0, obj_equip_fists);

with(obj_equip_fists)
{
	// owner of fist weapons
	owner = self;
}

// Set the player character to this
global.playerObject = self;
// Print the player object
show_debug_message(object_get_name(global.playerObject.object_index) + " is the Player Object.");