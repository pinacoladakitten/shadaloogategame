/// @description Insert description here
// You can write your code in this editor

// Set cursor to none to hide for sprite
window_set_cursor(cr_none);

// Examine command type icon when selecting between the main 3 commands
examine_type = "NONE";

// Selection for subcommands under those 3 commands
selection = "none";

// Enable Clicking on Actions
bisActive = true;

// If has item selected
hasItem = false;

// Delayed Create
alarm_set(0, 60);