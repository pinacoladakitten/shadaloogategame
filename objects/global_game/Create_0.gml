/// @description Setup Default global stuff...
// On object re-creation (mainly for battle creation)

global.playerObject = "";
global.EnemyPool = ds_list_create();

// Set enemy pool
ds_list_add(global.EnemyPool, obj_enm_bat01);

// Maximum Enemies
global.maxEnemySpawn = 4;