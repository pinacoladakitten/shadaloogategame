/// @description Insert description here
// On Game Start


// Screen shake effect
shake = false;
view_x = camera_get_view_x(view_camera[0]);
view_y = camera_get_view_y(view_camera[0]);

// Controls
global.key_up = ord("W");
global.key_down = ord("S");
global.key_left = ord("A");
global.key_right = ord("D");

// Global Game Events
global.gameEvents = ds_map_create();
global.gameEvents[? "boot"] = 1;

// Game Data
global.inventory = ds_list_create();
global.cursor = undefined;
global.displayInventory = undefined;