/// @description When executed
// You can write your code in this editor

// Print the target if valid
if(global.playerObject.target != "")
{
	// Print
	show_debug_message(object_get_name(global.playerObject.target.object_index) + " is the target.");
	
	// Disable clicking
	obj_player_cursor.bisActive = false;
}