/// @description When clicked on (Set cursor selection type)
// You can write your code in this editor
if(instance_exists(obj_player_cursor))
{
	if(obj_player_cursor.bisActive == true)
	{
		obj_player_cursor.selection = commandType;
	
		// Excecute when clicked (10 frame delay)
		alarm_set(0, 10);
	}
}