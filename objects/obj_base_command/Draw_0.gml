/// @description Draw command box
// You can write your code in this editor

// Check if cursor exists
if(instance_exists(obj_player_cursor))
{
	draw_set_font(Font1);
	// See if selection == command type
	if(obj_player_cursor.selection != commandType) // Not Selected
	{
		draw_sprite(spr_command_border, 0, x, y);
		draw_text_ext_transformed_color(x+10, y, commandType, 0, 150, 2, 2, 0, -1, -1, -1, -1, 1);
	}
	else	// Selected
	{
		draw_sprite(spr_command_border, 1, x, y);
		draw_text_ext_transformed_color(x+10, y, commandType, 0, 150, 2, 2, 0, 0, 0, 0, 0, 1);
	}
}