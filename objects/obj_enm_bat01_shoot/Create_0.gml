/// @description Init Vars
// You can write your code in this editor
//Inherited
event_inherited();

// Move stuff
move_angle = irandom_range(0, 360);

// Image scaling and speed
image_xscale = 0.75;
image_yscale = 0.75;
image_speed = 0.5;

// Object speed
move_speed = 2;

// Bounce
randomize();
move_towards_point(irandom_range(-360, 360), irandom_range(-10, 10), move_speed);

// Set name
name = "bat";