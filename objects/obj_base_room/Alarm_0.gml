/// @description Damage Characters on Attack Success
// You can write your code in this editor
show_debug_message("Attacks Commencing...");

// Loop through the enemy list and see if their attacks are successful or not
if(i_attk < ds_list_size(EnemyList) && ds_list_size(EnemyList) > 0)
{
	
	// Deal damage to enemy target or if enemy attacks player
	DealDamage(EnemyList[| i_attk]);
	
	// increment the loops based on the alarm timer
	i_attk++;
	alarm_set(0, 0.5*room_speed);
}
else
{
	
	// Reset our loop count
	i_attk = 0;
	
	// Set the cursor back to defaults
	with(obj_player_cursor)
	{
		bisActive = true;
		selection = "none";
		examine_type = "NONE";
	}
	
	// remove any commands
	with(obj_base_examineType)
	{
		event_user(0);
	}
}