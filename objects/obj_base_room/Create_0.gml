/// @description Init Variables, Set depth, Stop audio,
// You can write your code in this editor
depth = 100;

// Event handling (by default set the player active to false)
if instance_exists(obj_player_cursor)
{
	obj_player_cursor.bisActive = false;
}

// Enemy List
EnemyList = ds_list_create();

// Set default background
var lay_id = layer_get_id("EffectBG");
var back_id = layer_background_get_id(lay_id);
layer_background_sprite(back_id, -1); // No BG
layer_background_blend(back_id, 0);   // Black BG
layer_background_alpha(back_id, 0);

// Attacking Loop Count when looping through to see who attacks
i_attk = real(0);