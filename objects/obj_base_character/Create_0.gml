/// @description Init Vars
// You can write your code in this editor

// Get Character Name
name = "Yourself.";
	
// Get Character hp and mp
hpMax = 10;
mpMax = 10;
hp = hpMax;
mp = mpMax;
	
// Get Character attk, mag, and armor Bns
attkBns = 2;
magBns = 4;
armor = 0;
bulletBns = 0;
	
// Get Character inventory
inventory[0] = "";
	
// Target object
target = "";

// Set sprite depth to go under or over shit...
depth = 10;

// Weapons or Items to equip and use
mainHand = "";
offHand = "";

// Compass object for shooting
CompassObj = "";

// If the character is successful attacking (if true, then attk player, if false then get attacked)
bisAttackSuccess = true;

// Is Damaged Variables
bisDamaged = false;