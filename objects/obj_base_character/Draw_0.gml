/// @description Draw character
// You can write your code in this editor
draw_sprite_ext(sprite_index, -1, x, y, 2, 2, 0, image_blend, 1);

draw_set_font(Font1);
draw_set_halign(fa_middle);
draw_text_ext_transformed(x, y-100, hp, 0, 150, 2, 2, 25);
draw_set_halign(fa_left);