/// @description When clicked on by target mouse
// You can write your code in this editor

if(obj_player_cursor.bisActive)
{
	global.playerObject.target = self;
	// Print
	show_debug_message(object_get_name(global.playerObject.target.object_index) + " is now the target.");
}