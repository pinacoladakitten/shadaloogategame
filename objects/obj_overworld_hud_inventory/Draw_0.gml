/// @description Draw items in inventory, main hand, and off hand
// You can write your code in this editor

// Border
draw_sprite_ext(sprite_index, 0, x, y, 1, 1, 0, -1, 1);

// Inventory Text
draw_sprite_ext(spr_inventory, 0, x+560, y+110, 1.5, 1.5, 0, -1, 1);

draw_set_font(Font2);
// Main Hand Equip
draw_text_ext_transformed_color(x+480, y+100, "Main", 0, 150, 2, 2, 0, -1, -1, -1, -1, 1);
draw_sprite_ext(spr_equipBox, 0, x+480, y+5, 1.5, 1.5, 0, -1, 1);

// Off Hand Equip
draw_text_ext_transformed_color(x+630, y+100, "Off", 0, 150, 2, 2, 0, -1, -1, -1, -1, 1);
draw_sprite_ext(spr_equipBox, 0, x+630, y+5, 1.5, 1.5, 0, -1, 1);