/// @description On Delayed Create
// You can write your code in this editor

// set the game inventory to this
ds_list_add(global.inventory, obj_inv_item);

// set display inventory to this
global.displayInventory = self;

// Refresh list
if(global.displayInventory != undefined)
{
	// This just refreshes itself
	with(global.displayInventory){
		event_user(0);
	}
}
