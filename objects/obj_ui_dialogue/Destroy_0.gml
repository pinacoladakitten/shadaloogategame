/// @description Allow movement to the cursor again
// You can write your code in this editor
if(bAllowMovementEnd)
{
	obj_player_cursor.bisActive = true;
}

if(instance_exists(ownerID.object_index) && ownerID != "")
{
	// Display owner if valid
	show_debug_message("Textbox Owner: " + object_get_name(ownerID.object_index));
	
	// Call event user 15 whenever the owner wants to do something after text box is destroyed
	with(ownerID)
	{
		// --> CALLBACK to a specific event from the owner to execute once text is done...
		event_user(15); // Make sure this is called after everything in here
	}
}