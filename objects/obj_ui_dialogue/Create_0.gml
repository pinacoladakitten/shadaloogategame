/// @description Create Variables and Init Text Array
// You can write your code in this editor

// Text array
text[0] = "Hello World";
text[1] = "This is a really, really, really, long and silly string to test that the line wrapping works okay.";

// Text variables
text_current = 0;
text_last = 1;
text_width = 300;

// Char variables
char_current = 0;
char_speed = 0.5;

// Wrap Text
text[text_current] = string_wrap(text[text_current], text_width);

// Preset X and Y Vars
x = 384;
y = 448;

// Alarm for playing sounds
alarm_set(0, room_speed*0.1);

// Alarm for being able to skip text
alarm_set(1, room_speed*0.1);
can_skip_text = false;

// Current string length
str_len = 0;

// Allow movement on destroy
bAllowMovementEnd = true;

// Do something with text box's creator when destroyed
ownerID = "";

// SETTINGS
play_no_char_sound = true;
yOffset = 0;
xOffset = 0;
appliedOffsets = false;
depth = -100;