/// @description On Text Advance
// You can write your code in this editor

// return if can't use text skip
if(!can_skip_text)
{
	alarm_set(1, room_speed*0.1);
	return;
}

var _len = string_length(text[text_current]);
if (char_current < _len)
{
	char_current = _len;
}
else
{
	text_current += 1;
	if (text_current > text_last)
	{
		instance_destroy();
	}
	else
	{
		text[text_current] = string_wrap(text[text_current], text_width);
		char_current = 0;
	}
}