/// @description Draw Text
// You can write your code in this editor

// Offset UI
if(!appliedOffsets)
{
	x+=xOffset;
	y+=yOffset;
	appliedOffsets = true;

}
// Draw Border
draw_sprite_ext(sprite_index, -1, x, y, 5, 2, 0, -1, 1);

// Draw Text
draw_set_font(Font2);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

str_len = string_length(text[text_current]);
if (char_current < str_len)
{
	char_current += char_speed;
}

var _str = string_copy(text[text_current], 1, char_current);
draw_text_ext_transformed_color(x, y-5,  _str, 20, 300, 2, 2, 0, c_yellow, c_yellow, c_yellow, c_yellow, 1);