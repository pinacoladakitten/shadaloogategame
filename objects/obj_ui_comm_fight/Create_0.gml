/// @description Setup variables for FIGHT
// Execute Inherited Events
event_inherited();

// Add Initial Commands to Attack Array
ds_list_add(CommandArry, obj_command_attack);

// Examine Type Name
ExamineTypeName = "FIGHT";

// Examine Type Display Name
DisplayName = "Fight";