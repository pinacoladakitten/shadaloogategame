/// @description When enemy list is empty
// You can write your code in this editor

if(bCanShoot)
{
	if(enemy_cnt == 0 || bullet_shots == 0 || life < 2)
	{
		// Set active
		bCanShoot = false;
	
		// Create Textbox
		var textBox = instance_create_depth(room_width/2, room_height/2, 1, obj_ui_dialogue);
	
		// Set owner
		textBox.ownerID = id;
	
		// Set text for it
		with(obj_ui_dialogue)
		{
			getEquipItemUse("COMPASS");
			bAllowMovementEnd = false;
		}
	}
}