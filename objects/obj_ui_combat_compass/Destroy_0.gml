/// @description Destroy enemy list
// You can write your code in this editor
for(var i = 0; i < array_length(EnemyCompList); i++)
{
	with(EnemyCompList[i])
	{
		instance_destroy();
	}
}


// Destroy borders
if(border_in)
{
	instance_destroy(border_in);
}
if(border_out)
{
	instance_destroy(border_out);
}