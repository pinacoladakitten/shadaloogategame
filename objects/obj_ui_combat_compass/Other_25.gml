/// @description CALLBACK-On Dialogue Box Destroyed
// You can write your code in this editor
with(obj_base_room)
{
	// Set alarm for each character to take damage
	alarm_set(0, 0.5*room_speed);
	
	// Set active
	bisActive = false;
}
instance_destroy();