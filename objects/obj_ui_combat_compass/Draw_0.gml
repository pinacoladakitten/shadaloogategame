/// @description Insert description here
// You can write your code in this editor

// Draw the border
draw_sprite_ext(sprite_index, -1, x, y, image_xscale*2, image_yscale*2, 0, -1, 1);

// Draw the compass for shooting
if (bisActive)
{
	// Get the compass rotations
	if(compass_rot == 20 || compass_rot == -20)
		compass_dir *= -1;
	
	compass_rot += 0.1*compass_dir;

	compass_angle = point_direction(xx,yy, obj_player_cursor.x, obj_player_cursor.y);

	// Rotate the compass sprite and draw it
	draw_sprite_ext(spr_combat_compass_pt2, -1, xx, yy, 2, 2, compass_angle-90, -1, 1);
	draw_sprite_ext(spr_combat_compass_pt1, -1, xx, yy, 2, 2, compass_rot, -1, 1);
	draw_sprite_ext(spr_targ_line, -1, xx, yy, 2, 2, compass_angle-90, -1, 0.2);

	draw_set_font(Font2);
	// Bullets
	draw_text_ext_transformed(x+10-sprite_width, y+10-sprite_height, "Bullets: ", 0, 150, 2, 2, 0);
	draw_text_ext_transformed(x+150-sprite_width, y+10-sprite_height, bullet_shots, 0, 150, 2, 2, 0);
	
	// Life
	draw_text_ext_transformed(x+10-sprite_width, y+450-sprite_height, "Life: ", 0, 150, 2, 2, 0);
	draw_text_ext_transformed(x+100-sprite_width, y+450-sprite_height, life, 0, 150, 2, 2, 0);
	
	// Combo
	draw_text_ext_transformed(x-200+sprite_width, y+10-sprite_height, "Combo: " + string(combo_dmg), 0, 150, 2, 2, 0);
}