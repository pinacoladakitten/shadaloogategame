/// @description Spawn Enemies
// You can write your code in this editor

// Get Enemies to Spawn
if(instance_exists(obj_base_room))
{
	for(var i = 0; i < ds_list_size(obj_base_room.EnemyList); i++)
	{
		//  get random location
		var xx = irandom_range(x-sprite_width+60, x+sprite_width-60);
		// spawn the enemies and add them to enemy list
		var enm = instance_create_depth(xx, y-100, 4, obj_base_room.EnemyList[| i].CompassObj);
		EnemyCompList[i] = enm;
		
		// show debug message
		show_debug_message("Enemies Spawned: " + enm.name);
		
		// Increase enemy count
		enemy_cnt++;
		//Increase bullets we can shoot
		bullet_shots++;
		
		// Set the enemy's compass obj to this
		enm.compass_obj = id;
			
		// set their parents to damage if hit
		enm.parentChar = obj_base_room.EnemyList[| i].id;
	}
}