/// @description Insert description here
// You can write your code in this editor

if(bisActive && bCanShoot && bullet_shots > 0)
{
	// Decrement Bullets
	bullet_shots--;
	
	// Set the offset to spawn bullets from the tip of the pointer
	spawn_len = point_distance(xx, yy, xx, yy-50);
	spawn_angle = point_direction(xx, yy, xx, yy-50);
	
	// Calculate the spawn point based off of this offset
	var pspawn_x = xx + lengthdir_x(spawn_len,spawn_angle+compass_angle-90);
	var pspawn_y = yy + lengthdir_y(spawn_len,spawn_angle+compass_angle-90);

	// Create bullet
	instance_create_depth(pspawn_x, pspawn_y, 5, obj_ui_compass_bullet);
	audio_play_sound(snd_comp_shoot01, 10, false);
	
	// Screen shake
	with(global_game)
	{
		screenShake(5);
	}
}