/// @description Insert description here
// You can write your code in this editor

show_debug_message("Compass Combat Created");

// Set depth
depth = 5;

// Compass Rotation Anim
compass_rot = 0;
compass_dir = -1;

// Active or not
bisActive = false;
bCanShoot = true;

// Scale size spawn anim
image_yscale = 0;

// Create Outer Border Collision
border_out = instance_create_depth(x, y, 0, obj_ui_border_col_out);
border_out.image_xscale = 2;
border_out.image_yscale = 2;

// Create Inner Border Collision
border_in = instance_create_depth(x, y, 0, obj_ui_border_col_in);
border_in.image_xscale = 0.3;
border_in.image_yscale = 0.3;
border_in.compass_obj = id;

// Compass Coords
xx = x;
yy = y;

// Combo Mechanic
combo_dmg = 0;

// Enemy Spawn List Init
enemy_cnt = 0;

// Amount of shots
bullet_shots = 0;

// Enemy Compass List
EnemyCompList[0] = "";

// Compass Shooting Angle
compass_angle = 0;

// Compass health
life = 2;

// Spawn Enemies
event_user(0);