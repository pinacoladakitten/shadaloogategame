/// @description Spawn in anim
// You can write your code in this editor

if (image_yscale < 1)
	image_yscale+=0.05;
else
	bisActive = true;


if(bCanShoot)
{
	if(keyboard_check(global.key_up)){yy-=2;}
	if(keyboard_check(global.key_down)){yy+=2;}
	if(keyboard_check(global.key_left)){xx-=2;}
	if(keyboard_check(global.key_right)){xx+=2;}
}

if(border_in)
{
	border_in.x = xx;
	border_in.y = yy;
}