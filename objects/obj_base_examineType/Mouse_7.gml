/// @description When this object is clicked on

// See if the command is clicked on
if(instance_exists(obj_player_cursor))
{
	// See if active or not
	if (!obj_player_cursor.bisActive) return;
		
	// Draw the commands once selected
	if(obj_player_cursor.examine_type != ExamineTypeName)
	{
		// Then create commands array
		for(var i = 0; i < ds_list_size(CommandArry); i++)
		{
			if(CommandArry[| i])
			{
				// Create command and show that it was created
				CommandInstanceArry[| i] = instance_create_depth(x, y+32*i, 10, CommandArry[| i]);
				show_debug_message(string(CommandInstanceArry[| i]) + " Created");
			}
		}
			
		// Set the cursor command to Command Selected
		obj_player_cursor.examine_type = ExamineTypeName;
	}
}