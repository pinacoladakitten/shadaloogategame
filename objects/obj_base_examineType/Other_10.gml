/// @description Once not clicked on
// You can write your code in this editor

show_debug_message("Examine Type: " + obj_player_cursor.examine_type);

// Destroy commands once not selected
if(obj_player_cursor.examine_type != ExamineTypeName)
{
	for(var i = 0; i < ds_list_size(CommandInstanceArry); i++)
	{
		if(CommandInstanceArry[| i])
		{
			if(instance_exists(CommandInstanceArry[| i]))
			{
				show_debug_message(string(CommandInstanceArry[| i]) + " Deleted")
				instance_destroy(CommandInstanceArry[| i]);
			}
		}
	}
	
	// Clear the Command Instances
	if(ds_list_size(CommandInstanceArry) > 0)
		ds_list_clear(CommandInstanceArry);
}