/// @description Setup varaibles for EXAMINE TYPE
// You can write your code in this editor
depth = 10;

// Setup examine commands

// Commands to hold in inventory
CommandArry = ds_list_create();

// Commands spawned in the menu to select
CommandInstanceArry = ds_list_create();

// Examine Type Name
ExamineTypeName = "NADA";

// Examine Type Display Name
DisplayName = "Nada";