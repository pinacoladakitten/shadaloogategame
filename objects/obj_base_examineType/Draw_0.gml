/// @description Draw the examine type or draw its commands
// You can write your code in this editor

if(instance_exists(obj_player_cursor))
{
	if(obj_player_cursor.examine_type != ExamineTypeName)
	{
		draw_set_font(Font1);
		draw_sprite_ext(sprite_index, 0, x, y, image_xscale, image_yscale, 0, -1, 1);
		draw_text_ext_transformed_color(x+25, y+115, DisplayName, 0, 150, 2, 2, 0, 0, 0, 0, 0, 1);
	}
}