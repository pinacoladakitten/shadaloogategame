/// @description When this is clicked
// When this is clicked
event_inherited();

// Set the text box text (needs with statement...)
if(instance_exists(self.textBox))
{
	// == No item override text ==
	if(!itemOverride)
	{
		// ... sets the text here
		textBox.text[0] = "I feel pretty ok.";
	
		// The final text in the list
		textBox.text_last = 0;
	}
}
