/// @description Insert description here
// You can write your code in this editor

// Set font
draw_set_font(Font1);

// Character portrait
draw_sprite_ext(sprite_index, 0, x, y+portBounce, image_xscale, image_yscale, 0, image_blend, 1);

// Port Border
draw_sprite_ext(spr_char_port_border, 0, x, y+2, image_xscale, image_yscale, 0, -1, 1);

// HP text
draw_sprite_ext(spr_hp, 0, x+10, y+225, image_xscale, image_yscale, 0, -1, 1);
//HP value
draw_text_ext_transformed_color(x+130, y+275, hp, 0, 150, 3, 3, 0, -1, -1, -1, -1, 1);

// MP text
draw_sprite_ext(spr_mp, 0, x+10, y+275, image_xscale, image_yscale, 0, -1, 1);
//MP value
draw_text_ext_transformed_color(x+130, y+325, mp, 0, 150, 3, 3, 0, -1, -1, -1, -1, 1);

// Target value (Can only be a character)
if(target != "")
{
	if(object_get_parent(target.object_index) == obj_base_character)
	{
		draw_set_font(Font1);
		draw_text_ext_transformed_color(x+130, y+475, target.name, 0, 150, 2, 2, 0, -1, -1, -1, -1, 1);
	}
}

// Target text
draw_set_font(Font2);
draw_text_ext_transformed_color(x+20, y+470, "Target:", 0, 150, 2, 2, 0, -1, -1, -1, -1, 1);