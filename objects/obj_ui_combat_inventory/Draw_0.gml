/// @description Draw items in inventory, main hand, and off hand
// You can write your code in this editor
draw_sprite_ext(sprite_index, 0, x, y, 2, 2, 0, -1, 1);

draw_set_font(Font2);
// Main Hand Equip
draw_text_ext_transformed_color(x+10, y-120, "Main", 0, 150, 2, 2, 0, -1, -1, -1, -1, 1);
draw_sprite_ext(spr_equipBox, 0, x+10, y-80, 1.5, 1.5, 0, -1, 1);

// Off Hand Equip
draw_text_ext_transformed_color(x+150, y-120, "Off", 0, 150, 2, 2, 0, -1, -1, -1, -1, 1);
draw_sprite_ext(spr_equipBox, 0, x+150, y-80, 1.5, 1.5, 0, -1, 1);