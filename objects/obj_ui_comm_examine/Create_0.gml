/// @description Setup variables for EXAMINE
// Execute Inherited Events
event_inherited();

// Examine Type Name
ExamineTypeName = "EXAMINE";

// Examine Type Display Name
DisplayName = "Examine";