/// @description Insert description here
// You can write your code in this editor
var textBox = instance_create_depth(x, y, 1, obj_ui_dialogue);

// Set the text box text (needs with statement...)
with(textBox)
{
	rm000_dialogue_dia01();
}
// Set owner to self
textBox.ownerID = id;