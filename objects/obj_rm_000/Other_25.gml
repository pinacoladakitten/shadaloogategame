/// @description CALLBACK-On Dialogue Box Destroyed
// You can write your code in this editor
// Play music
audio_play_sound(ShadowGateTheme, 10, false);
	
// Enable player movement
if instance_exists(obj_player_cursor)
{
	obj_player_cursor.bisActive = true;
}