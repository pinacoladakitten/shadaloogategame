/// @description Init Vars

// Execute inherited
event_inherited();

// You can write your code in this editor
alarm_set(0, room_speed*5);

// Audio stuff...
audio_stop_all();
audio_play_sound(cave_amb01, 0, false);