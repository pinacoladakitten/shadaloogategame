/// @description Mouse Enter
// You can write your code in this editor

// Change cursor icon
if(instance_exists(obj_player_cursor))
{
	// See if active or not
	if (!obj_player_cursor.bisActive) return;
	if (obj_player_cursor.hasItem) return;
	
	obj_player_cursor.examine_type = "EXAMINE";
}