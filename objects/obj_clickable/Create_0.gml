/// @description On Create
// On Create

// vars
self.textBox = undefined;
itemOverride = false;

// item reactions, overrides what this clickable would normally due, since we have an item selected
// default item
item =
{
	name : "item",
	text: ["What you expected to happen...didn't."]
}

itemCombines[0] = item;