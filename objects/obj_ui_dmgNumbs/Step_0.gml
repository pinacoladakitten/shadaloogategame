/// @description Move stuff
// You can write your code in this editor

// Move in a direction
x += moveSpeed;
y += moveSpeed/2;

//Slow down
moveSpeed = max(moveSpeed-1, 0);

if(moveSpeed <= 0)
{
	image_alpha -= 0.05;
}