/// @description Init Vars
// You can write your code in this editor

// Inherit the parent event
event_inherited();

// Get Character Name
name = "Bat";
	
// Get Character hp and mp
hpMax = 3;
mpMax = 3;
hp = hpMax;
mp = mpMax;
	
// Get Character attk, mag, and armor Bns
attkBns = 3;
magBns = 1;
armor = 0;
	
// Get Character inventory
inventory[0] = "";
	
// Target object
target = global.playerObject;

// Set size and speed
image_speed = 0.25;
image_xscale = 2;
image_yscale = 2;

// Set compass obj
CompassObj = obj_enm_bat01_shoot;
