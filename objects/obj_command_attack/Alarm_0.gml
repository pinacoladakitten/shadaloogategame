/// @description When executed
// You can write your code in this editor

// Execute inherited
event_inherited();

// DO command stuff to player target
if(global.playerObject.target != "")
{
	// Spawn text box
	var textBox = instance_create_depth(room_width/2, room_height/2, 1, obj_ui_dialogue);
	
	// Set owner
	textBox.ownerID = id;
	
	// Set text for it
	with(obj_ui_dialogue)
	{
		getEquipItemUse("ATTACK");
		bAllowMovementEnd = false;
	}
	
	// Stop player control
	obj_player_cursor.bisActive = false;
}