/// @description Insert description here
// You can write your code in this editor

// Create textbox only if we have another item selected
if(!instance_exists( self.textBox ))
{
	// Set the text box text (needs with statement...)
	// == Item selected override text ==
	itemOverride = false;
	if(instance_exists(global.cursor))
	{
		if(global.cursor.hasItem && display_name != global.cursor.examine_type)
		{
			self.textBox = instance_create_depth(x, y, 1, obj_ui_dialogue);

			// Set owner to self
			self.textBox.ownerID = id;
			self.textBox.play_no_char_sound = false;
			self.textBox.yOffset = 100;
	
			itemOverride = true;
			var foundItem = false;
			// Get the selected item
			for(i = 0; i < array_length(itemCombines); i++)
			{
				if(itemCombines[i].name == global.cursor.examine_type)
				{
					// override textbox text
					textBox.text = itemCombines[i].text;
					textBox.text_last = array_length(textBox.text)-1;
					foundItem = true;
				}
			}
			
			// didn't find an item override, do nothing
			if(!foundItem)
			{
				textBox.text = [];
				textBox.text[0] = "Nothing happens."
				textBox.text_last = array_length(textBox.text)-1;
			}
		}
		else{
			global.cursor.examine_type = "NONE";
		}
	}
		
	// == No Item, then use normal text ==
	if(!itemOverride)
	{
		// do nothing...
	}
}