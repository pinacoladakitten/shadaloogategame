/// @description Insert description here
// You can write your code in this editor

if(position_meeting(mouse_x, mouse_y, id))
{
	if(instance_exists(global.cursor))
	{
		global.cursor.sprite_index = icon;
		global.cursor.examine_type = self.display_name;
		global.cursor.hasItem = true;
	}
}
else
{
	if(instance_exists(global.cursor))
	{
		if(global.cursor.examine_type == self.display_name)
		{
			global.cursor.examine_type = "NONE";
			global.cursor.hasItem = false;
		}
	}
}