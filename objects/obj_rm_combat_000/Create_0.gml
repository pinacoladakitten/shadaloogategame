/// @description Init Vars

// Execute inherited
event_inherited();

// Audio stuff...
audio_stop_all();
//audio_play_sound(cave_amb01, 0, false);

// Create textbox
var textBox = instance_create_depth(x, y, 1, obj_ui_dialogue);

// Set the text box text (needs with statement...)
with(textBox)
{
	rm_combat000_dia01();
}
// Set owner to self
textBox.ownerID = id;

// Set the room background
var lay_id = layer_get_id("EffectBG");
var back_id = layer_background_get_id(lay_id);
layer_background_sprite(back_id, sprite_index);
layer_background_blend(back_id, -1);
layer_background_alpha(back_id, 0.75);

// Spawn Enemies
randomize();
var enemyNumber = irandom_range(1, global.maxEnemySpawn);
for(var i = 0; i < enemyNumber; i++)
{
	var j = irandom_range(0, ds_list_size(global.EnemyPool)-1);
	ds_list_add(EnemyList, instance_create_depth(x+50+200*i, sprite_height/2, 10, global.EnemyPool[| j]));
}