{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "cave_amb01.ogg",
  "duration": 5.131519,
  "parent": {
    "name": "rm_intros",
    "path": "folders/Sounds/rm_intros.yy",
  },
  "resourceVersion": "1.0",
  "name": "cave_amb01",
  "tags": [],
  "resourceType": "GMSound",
}