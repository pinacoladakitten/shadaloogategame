{
  "compression": 0,
  "volume": 0.29,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_comp_shoot01.wav",
  "duration": 0.269796,
  "parent": {
    "name": "sfx",
    "path": "folders/Sounds/sfx.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_comp_shoot01",
  "tags": [],
  "resourceType": "GMSound",
}