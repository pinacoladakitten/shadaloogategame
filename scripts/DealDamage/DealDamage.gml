/// @function                   DealDamage(targetChar);
/// @param  {string}			targetChar    The enemy to damage
/// @description				Damage either the enemy or player based on what the compass outcome is
function DealDamage(targetChar)
{
	
	// Init Dmg
	var damage = 0;
	
	if(targetChar.bisAttackSuccess == true)		// Deal damage to player
	{
		damage = targetChar.attkBns;		// get damage
		global.playerObject.hp -= damage;	// subtract the damage
		show_debug_message("Player takes: " + string(damage)); // display
		
		// Set player to have damaging effects
		targetChar = global.playerObject;
	}
	else	// Deal player damage to enemy
	{
		damage = global.playerObject.attkBns;
		
		// Calculate DMG
		if(targetChar.id == global.playerObject.target.id){
			damage *= 2;		// Targetted Damage
		}
		else{
			damage /= 2;		// Non-targetted Damage
		}
		
		// Deal the DMG
		targetChar.hp -= damage;
	}
	
	// Do damaging effects to whoever was targeted with the hit
	with(targetChar)
	{
		event_user(14);
	}
		
	// Display DMG
	instance_create_depth(targetChar.x, targetChar.y, 10, obj_ui_dmgNumbs);
	with(obj_ui_dmgNumbs){
		dmgAmt = damage;
	}
		
	show_debug_message("Char takes: " + string(damage));
		
	// Set enemy attacks back
	targetChar.bisAttackSuccess = true;
}