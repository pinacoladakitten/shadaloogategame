// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function rm000_dialogue_dia01(){
	text[0] = "The sound of twisting winds suddenly wake you from a long nap."
	text[1] = "Your eyes rush open only to find a gigantic cave which encapsulates your entire view."
	text[2] = "Memories suddenly come rushing back to you, 'Destroy it! Destroy it please I'm begging you!'."
	text[3] = "It's The King! The King is telling you this, and he's saying to destroy something..."
	text[4] = "'...but what DID he want me to destroy?', you think to yourself."
	text[5] = "'Bah! That doesn't matter, if this is a threat to my homeland then it must be wiped out!'"
	text[6] = "~And so your quest begins, here.~"
	
	text_last = 6;
}