/// @function                   screenShake(shakeTimer);
/// @param  {real}			    shakeTimer The amt of time to shake in frames
/// @description				Set the amt of time to screen shake then shake for that time

/// FOR USE WITH GLOBAL GAME OBJ
function screenShake(shakeTimer)
{
	// Shake timer
	alarm_set(0, shakeTimer);
	// Set shake to true
	shake = true;
}