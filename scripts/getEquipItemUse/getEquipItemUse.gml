/// @function                   getEquipItemUse(itemHashName);
/// @param  {string}			itemHashName    The item's text to get on usage
/// @description				Put in an items hash name in order to print out the text when using it
function getEquipItemUse(itemHashName)
{
	switch(itemHashName)
	{
		case "ATTACK":
			text[0] = "You prepare yourself for an attack with all your might!";
			text_last = 0;
		break;
		
		case "COMPASS":
			text[0] = "The compass has decided your fate!";
			text_last = 0;
		break;
		
		default:
			var textType = irandom_range(1, 2);
			
			switch(textType)
			{
				case 1:
					text[0] = "You seem to be wasting your time.";
					text_last = 0;
				break;
				case 2:
					text[0] = "You expected something to happen, only to be unfortunately disappointed.";
					text_last = 0;
				break;
			}
		break;
	}
}